#!/bin/sh

# -----------------------------------------------------------------------------
# Copyright (C) 2018 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

# Health-check - return 0 if healthy, 1 otherwise.
#
# Gynack writes a 'gynack.error' file with error details if something has
# gone seriously wrong. If this file exists the health-check will return a
# non-zero error code.

if [ -f gynack.error ]; then
    exit 1
fi

# Healthy if we get here...
exit 0
