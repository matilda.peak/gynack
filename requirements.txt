aiohttp == 3.3.2
apscheduler == 3.5.3
pytz == 2019.1
PyYAML == 5.1.1
requests == 2.22.0
