#!/bin/sh

# -----------------------------------------------------------------------------
# Copyright (C) 2018 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

# Start the utility
python gynack.py
