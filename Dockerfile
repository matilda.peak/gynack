# -----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

ARG from_image=python:3.7.4-alpine3.10
FROM ${from_image}

# Labels
LABEL maintainer='Matilda Peak <info@matildapeak.com>'

# Install timezone info into Alpine image...
RUN apk add --no-cache tzdata
ENV TZ=UTC

# Force the binary layer of the stdout and stderr streams
# to be unbuffered
ENV PYTHONUNBUFFERED 1

# Base directory for the application
# Also used for user directory
ENV APP_ROOT /home/gynack

# Containers should NOT run as root
# (as good practice)
RUN adduser -D -h ${APP_ROOT} -s /bin/sh gynack
RUN chown -R gynack.gynack ${APP_ROOT}

COPY requirements.txt ${APP_ROOT}/
RUN pip install -r ${APP_ROOT}/requirements.txt

COPY gynack.py ${APP_ROOT}/
COPY datapoint/ ${APP_ROOT}/datapoint/
COPY image-config/logging.yml ${APP_ROOT}/
COPY LICENCE.txt ${APP_ROOT}/
COPY docker-entrypoint.sh ${APP_ROOT}/
COPY docker-healthcheck.sh ${APP_ROOT}/

RUN chmod 755 ${APP_ROOT}/docker-entrypoint.sh
RUN chmod 755 ${APP_ROOT}/docker-healthcheck.sh

USER gynack
ENV HOME ${APP_ROOT}
WORKDIR ${APP_ROOT}

CMD ./docker-entrypoint.sh
