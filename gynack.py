#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

"""`Gynack` is an experimental real-time data collector that is used to provide
demo material that can be used to build, experiment with and demonstrate
Chronicler.
"""

from datapoint import datapoint

datapoint.run()
