---
Title:      A Chronicler Data Feed (Gynack)
Author:     Alan Christie
Date:       13 July 2019
Copyright:  Matilda Peak. All rights reserved.
---

[![build status](https://gitlab.com/matilda.peak/gynack/badges/master/build.svg)](https://gitlab.com/matilda.peak/gynack/commits/master)

# The Gynack project
A feed of real-world real-time data for use with Chronicler
experiments. This early implementation collects [MetOffice] datapoint
observation data. Observations are available hourly from around 140
sites around the UK. 

## Running Gynack
Gynack is distributed as a container image but you can run it from the project
root after setting up a few things...

1.  You must have Python 3.7
1.  Ideally, create a Python [Conda] environment with something like
    `conda create -n gynack python=3.7`. You'll automatically be put into the
    new environment.
1.  Install the Python run-time requirements with
    `pip install -r requirements.txt`
1.  Run Gynack with `./gynack.py`

## Environment variables
You will need to define:

-   `MET_OFFICE_API_KEY` (your MetOffice application API key)
-   `GYNACK_CHRONICLER_URL`
-   `GYNACK_CHRONICLER_RESOURCE`

# Container Building
There's a Dockerfile to build the Docker image. Main building is done
via the GitLab CI service (see `.gitlab-ci.yml`) but you can build your own
images. For example, to build an image for the ARM processor: -

    $ docker build --build-arg from_image=arm32v7/python:3.7.4-alpine3.10 \
        -t registry.gitlab.com/matilda.peak/gynack:arm32v7-latest .

---

[Conda]: https://conda.io/docs/index.html
[MetOffice]: https://www.metoffice.gov.uk/datapoint/getting-started
