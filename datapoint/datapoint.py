#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2018 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

"""Gynack. A MetOffice data-point data collector for Chronicler.

The module collects observation data from the advertised set of locations
(148 individual locations at the time of writing) using `asyncio` and the
`aiohttp` modules.

You will need to set some environment variables before you can run this code.
You will need credentials for your MetOffice API registration:

-   MET_OFFICE_API_KEY

Additional, optional environment variables include the following:

-   GYNACK_CHRONICLER_URL   If set this is used as the base of the Chronicler
                            URL with bike date being transmitted to it. If not
                            set Chronicler is not used.
-   GYNACK_CHRONICLER_RESOURCE  The resource to use when transmitting data to
                                Chronicler.

NOTE:   The Chronicler connection has not been implemented yet!
"""

from decimal import Decimal
import os
import logging
from logging.config import dictConfig
import pprint
from collections import namedtuple
import signal
import sys
from datetime import datetime, timedelta
import json
import asyncio
import yaml
from aiohttp import ClientSession
import requests

from pytz import timezone

from apscheduler.schedulers.asyncio import AsyncIOScheduler

from datapoint.error import write_error

# Load logger configuration (from cwd)...
# But only if the logging configuration is present!
_LOGGING_CONFIG_FILE = 'logging.yml'
if os.path.isfile(_LOGGING_CONFIG_FILE):
    _LOGGING_CONFIG = None
    with open(_LOGGING_CONFIG_FILE, 'r') as stream:
        try:
            _LOGGING_CONFIG = yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)
    dictConfig(_LOGGING_CONFIG)

# Now it's configured
# (and it should be for all modules as `disable_existing_loggers`
# is set to False in `logging.yml`)
# we can now create our own logger. We use 'runner' as the logger here
# otherwise we'll get __main__ when we're run. We can use __main__ for all
# modules that are imported.
_LOGGER = logging.getLogger('datapoint')

# A pretty-print instance
_PP = pprint.PrettyPrinter(indent=2)

# Collection timezone.
# This has to be UTC
_TZ = timezone('UTC')

# How often locations are updated
# (value is number of observations where 24 is one day).
# Locations are always collected when the app starts.
_LOCATION_UPDATE_RATE = 24
_OBSERVATIONS_SINCE_UPDATE = 0
# Collect any observation backlog?
# If true the first collection will attempt to collect
# the expected 24-hour backlog of data ... the MetOffice
# makes observation data available for 24 hours.
_COLLECT_BACKLOG = False

# The MetOffice API KEY
# (mandatory - if you don't have this then you can't run this code)
_API_KEY_ENV = 'MET_OFFICE_API_KEY'
_API_KEY = os.environ.get(_API_KEY_ENV)
# The MetOffice base API
_API = 'http://datapoint.metoffice.gov.uk/public/data/'
# The DataPoint resource for locations and weather observations
_RESOURCE = 'val/wxobs/all/json'
# Authorisation (parameters) for the observation site list query
# and for individual observations (that require a 'resolution').
_SITE_LIST_AUTH = {'key': _API_KEY}
_SITE_AUTH = {'key': _API_KEY, 'res': 'hourly'}

# The Chronicler URL (optional).
# If not set we won't send to Chronicler.
_CHRONICLER_URL = os.environ.get('GYNACK_CHRONICLER_URL')
# Chronicler resource (fixed for now)
_CHRONICLER_RESOURCE = os.environ.get('GYNACK_CHRONICLER_RESOURCE')

# Keep the main-loop running...
_RUN = True

# The scheduler instance.
_SCHEDULER = None
# And the asyncio Event loop
_EVENT_LOOP = None

# A flag, cleared after the first data collection.
_FIRST_COLLECTION = True

# Location records (namedtuples)
_LOCATION = namedtuple('Location', ['id', 'name', 'lat', 'lon', 'ele'])
# The current list of locations
# (adjusted by regular calls to _update_locations())
_LOCATIONS = []


def _collect_locations():
    """Collects the list of observation locations.
    Normally run at startup and then at least once a day.
    It populates the global location list ``_LOCATIONS``.

    When collected the global list is replaced by the collected set.

    :return: The number of locations collected
    :rtype: ``int``
    """
    # pylint: disable=global-statement
    global _LOCATIONS
    global _RUN

    _LOGGER.info('Updating locations...')

    resp = None
    success = True
    try:
        resp = requests.get(os.path.join(_API, _RESOURCE, 'sitelist'),
                            params=_SITE_LIST_AUTH)
    except requests.exceptions.ConnectionError:
        success = False

    if success:
        new_locations = []
        locations = resp.json()['Locations']['Location']
        for location in locations:

            # If no longer running
            # break out of this potentially time-consuming loop...
            if not _RUN:
                break

            elevation = Decimal('nan')
            if 'elevation' in location:
                elevation = Decimal(location['elevation'])
            latitude = Decimal(location['latitude'])
            longitude = Decimal(location['longitude'])
            new_locations.append(_LOCATION(int(location['id']),
                                           location['name'],
                                           latitude, longitude, elevation))

        if _RUN:
            # Swap current set of locations with the new set
            _LOCATIONS = new_locations[:]
            _LOGGER.info('Found %s', len(_LOCATIONS))
        else:
            # Not running - no point in having any locations
            _LOGGER.info('Cleared location list')
            _LOCATIONS = []

    return len(_LOCATIONS)


async def _fetch_location_observation(url, params, session):
    """Gets a specific observation, returning the result.

    :param url: The location URL
    :type url: ``str``
    :param params: Optional URL parameters - but usually the API key and other material
    :type params: ``dictionary``
    :param session: The aiohttp sesssion
    :type session: ``ClientSession``
    """
    async with session.request('GET', url, params=params) as response:
        return await response.read()


async def _get_location_observations():
    """Attempts to get MetOffice observation data for all known locations.
    """
    # pylint: disable=global-statement
    global _FIRST_COLLECTION
    global _LOCATION_UPDATE_RATE
    global _OBSERVATIONS_SINCE_UPDATE
    global _SCHEDULER

    # If we don't have any locations try top get some.
    # Otherwise always try to get updated locations at the designated rate.
    if not _LOCATIONS or _OBSERVATIONS_SINCE_UPDATE >= _LOCATION_UPDATE_RATE:
        # Get the set of MetOffice Observation Locations.
        # We do this at start-up and regularly throughout the lifetime
        # of the service.
        _collect_locations()
        _OBSERVATIONS_SINCE_UPDATE = 0
    _OBSERVATIONS_SINCE_UPDATE += 1

    # What time is it now?
    # If this is the first execution then (if told to) we step back 24 hours
    # in order to collect the previous 24 hours worth of results.
    # Met Office maintains up to 24 hours of observations.
    utc_now = datetime.utcnow()
    num_hours = 1
    if _FIRST_COLLECTION and _COLLECT_BACKLOG:
        num_hours = 23
        utc_now -= timedelta(hours=num_hours)
    observation_time = None

    _LOGGER.info('Assembling weather observation requests...')
    tasks = []
    async with ClientSession() as session:

        while num_hours > 0:

            observation_time = datetime(year=utc_now.year,
                                        month=utc_now.month,
                                        day=utc_now.day,
                                        hour=utc_now.hour,
                                        tzinfo=_TZ)

            # Add the time to the dictionary of parameters.
            # The shortest representation will do (i.e. 2018-05-20T07Z)
            _SITE_AUTH['time'] = '%d-%02d-%02dT%02dZ' % (observation_time.year,
                                                         observation_time.month,
                                                         observation_time.day,
                                                         observation_time.hour)

            for location in _LOCATIONS:

                task = asyncio.ensure_future(
                    _fetch_location_observation(
                        os.path.join(_API, _RESOURCE, str(location.id)),
                        _SITE_AUTH,
                        session))
                tasks.append(task)

            num_hours -= 1
            if num_hours:
                utc_now += timedelta(hours=1)

        # Now make the calls...
        _LOGGER.debug('Gathering tasks...')
        responses = await asyncio.gather(*tasks)
        _LOGGER.debug('Gathered %s', len(responses))

        _LOGGER.debug('Processing...')
        for response in responses:
            json_response = json.loads(response.decode('utf-8'))
            if 'Location' in json_response['SiteRep']['DV']:
                data = json_response\
                    ['SiteRep']['DV']['Location']['Period']['Rep']
                data_date = json_response\
                    ['SiteRep']['DV']['dataDate']
                location_name = json_response\
                    ['SiteRep']['DV']['Location']['name']
                _LOGGER.debug('%s "%s" %s', data_date, location_name, data)
        _LOGGER.debug('Processed')

    _LOGGER.info('Processed %s weather observations', len(tasks))

    # Clear first collection flag.
    _FIRST_COLLECTION = False

    # Schedule a re-run of ourselves just after the next hour.
    assert observation_time
    observation_time += timedelta(hours=1, minutes=1)
    _LOGGER.info('Scheduling next collection for %s',
                 observation_time.isoformat())
    _SCHEDULER.add_job(_get_location_observations,
                       'date', run_date=observation_time)


def _exception_handler(loop, context):
    """An exception handler. To stop the event loop.
    """
    # pylint: disable=global-statement
    global _EVENT_LOOP

    # first, handle with default handler
    loop.default_exception_handler(context)

    if _EVENT_LOOP:
        _LOGGER.info('Stopping event loop...')
        # And write an error (Docker should kill us)
        write_error('Caught exception (%s)' % context)
        _EVENT_LOOP.stop()


def _signal_handler(signum, frame):
    """A signal handler, to handle things like CTRL-C.
    """
    # pylint: disable=global-statement
    global _RUN

    assert signum
    assert frame

    if _RUN:
        # All we have to do is clear the _RUN flag
        # which will release the _dispatch() thread.
        _LOGGER.warning('Caught signal (%s). Clearing _RUN...', signum)
        _RUN = False
        _LOGGER.info('Raising RuntimeError to kill the event loop...')
        # And write an error (Docker should kill us)
        write_error('Caught signal (%s)' % signum)
        # Raise an exception to stop the event loop.
        raise RuntimeError


def run():
    """The DataPoint collection entry point.

    This call blocks until the collection is stopped.
    """
    # pylint: disable=global-statement
    global _EVENT_LOOP
    global _SCHEDULER

    if _API_KEY:

        # Licence requires us to acknowledge TfL and Ordnance Survey.
        # Make sure this is emitted when the code runs.
        _LOGGER.info('-----------------------------')
        _LOGGER.info('MetOffice DataPoint Collector')
        _LOGGER.info('-----------------------------')
        _LOGGER.info('Powered by Met Office Data')
        _LOGGER.info('Contains public sector information')
        _LOGGER.info('licensed under the Open Government Licence')

        # Expose useful config...
        _LOGGER.info('------------------')
        _LOGGER.info('Hit CTRL-C to quit')
        _LOGGER.info('------------------')
        _LOGGER.info('_CHRONICLER_URL="%s"', _CHRONICLER_URL)
        _LOGGER.info('_CHRONICLER_RESOURCE="%s"', _CHRONICLER_RESOURCE)
        _LOGGER.info('------------------')

        # Attach a signal handler (for CTRL-C)
        signal.signal(signal.SIGINT, _signal_handler)

        # Continue
        # (unless user has already hit CTRL-C)
        if _RUN:

            # Create an asyncio scheduler...
            _SCHEDULER = AsyncIOScheduler()
            _SCHEDULER.start()

            # Trigger the first collection method (in a few seconds)...
            _SCHEDULER.add_job(_get_location_observations, 'date',
                               run_date=datetime.now() + timedelta(seconds=4))

            # Just park ourselves here (until the signal handler fires)
            _LOGGER.info('Entering event loop...')
            _EVENT_LOOP = asyncio.get_event_loop()
            _EVENT_LOOP.set_exception_handler(_exception_handler)
            try:
                _EVENT_LOOP.run_forever()
            except RuntimeError:
                _LOGGER.warning('Event loop stopped by RuntimeError')
            _LOGGER.info('Event loop done')

            # Stop everything we started...
            _SCHEDULER.shutdown()

        _LOGGER.info("That's all Folks!")

    else:

        _LOGGER.error('%s is undefined.'
                      ' You must provide an API key.', _API_KEY_ENV)
        write_error('No API key defined')
        sys.exit(1)
