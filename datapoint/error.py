#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2018 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

"""Writes to an error file. The docker health-check watches for the error
file, if it exists the health check considers Gynack to be unhealthy, and
Docker will stop the container.
"""

_ERROR_FILE = 'gynack.error'


def write_error(msg='unspecified'):
    """Writes the message to _ERROR_FILE.
    """
    with open(_ERROR_FILE, 'w') as file_obj:
        file_obj.write('%s\n' % msg)
